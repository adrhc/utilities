package ro.go.adrhc.util.fn;

import com.rainerhahnekamp.sneakythrow.functional.SneakyConsumer;
import com.rainerhahnekamp.sneakythrow.functional.SneakyFunction;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import static java.lang.Boolean.TRUE;
import static ro.go.adrhc.util.fn.FunctionUtils.failToEmpty;

@UtilityClass
@Slf4j
public class FunctionFactory {
	public static <P1, P2, R> Function<P2, R> rmP1(BiFunction<P1, P2, R> biFn) {
		return p2 -> biFn.apply(null, p2);
	}

	public static <T, R, E extends Exception>
	Function<T, Optional<R>> emptyFailResultFn(SneakyFunction<T, R, E> sneakyFn) {
		return t -> failToEmpty(sneakyFn, t);
	}

	public static <T, R, E extends Exception> Function<T, R>
	nullFailResultFn(SneakyFunction<T, R, E> sneakyFn) {
		return t -> {
			try {
				return sneakyFn.apply(t);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			return null;
		};
	}

	public static <T, R>
	Function<T, Optional<R>> optionalResultFn(Function<T, R> fn) {
		return t -> Optional.of(fn.apply(t));
	}

	public static <T> Function<T, String> stringResultFn(Function<T, ?> function) {
		return t -> {
			Object value = function.apply(t);
			return value == null ? null : value.toString();
		};
	}

	public static <T, E extends Exception> SneakyFunction<T, Boolean, E>
	finishWithTrue(SneakyConsumer<T, E> sneakyFn) throws E {
		return t -> {
			sneakyFn.accept(t);
			return TRUE;
		};
	}
}
