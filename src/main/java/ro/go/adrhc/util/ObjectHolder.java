package ro.go.adrhc.util;

public interface ObjectHolder<T> {
	T get();
}
