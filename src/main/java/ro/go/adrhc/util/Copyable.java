package ro.go.adrhc.util;

public interface Copyable<T> {
	T copy();
}
