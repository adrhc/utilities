package ro.go.adrhc.util.optional;

public interface OptionalStatus {
	boolean isPresent();

	boolean isMissing();
}
