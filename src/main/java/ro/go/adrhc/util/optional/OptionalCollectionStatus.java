package ro.go.adrhc.util.optional;

public interface OptionalCollectionStatus extends OptionalStatus {
	boolean isComplete();

	boolean isIncomplete();
}
